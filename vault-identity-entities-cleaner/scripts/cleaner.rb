#!/usr/bin/env ruby
# frozen_string_literal: true

# This script fetches all identities entities from Vault and delete those older
# than a given time (1 day by default).
# This is workaround for https://github.com/hashicorp/vault/issues/8761 until
# Vault gains the ability to do it by itself.

require 'logger'
require 'optparse'
require 'pathname'
require 'vault'

VAULT_REQUEST_MAX_ATTEMPTS = 5
KUBERNETES_SERVICE_ACCOUNT_TOKEN = Pathname.new('/var/run/secrets/kubernetes.io/serviceaccount/token').freeze

class Cleaner
  def initialize
    @logger = Logger.new($stdout)

    @options = {
      batch_size: 100,
      debug: false,
      dry_run: false,
      kubernetes_role: nil,
      limit: nil,
      age: 24 * 60 * 60 # 1 day
    }

    @vault_client = Vault::Client.new
    @vault_client.configure do |config|
      config.open_timeout = 2
      config.read_timeout = 30
      config.ssl_timeout  = 2
    end
  end

  def debug(msg)
    @logger.debug(msg) if @options[:debug]
  end

  def parse_opts
    OptionParser.new do |opts|
      opts.banner = "Usage: #{$PROGRAM_NAME} [options]"
      opts.on('-b', '--batch SIZE', Integer, 'Delete batch size. Default is 100.') do |b|
        @options[:batch_size] = b
      end
      opts.on('-d', '--days DAYS', Integer, 'Age of Vault identity entities to delete, in days. Default is 1 day.') do |d|
        @options[:age] = d * 24 * 60 * 60
      end
      opts.on('-l', '--limit COUNT', Integer, 'Maximum number of entities to delete. Default is unlimited.') do |l|
        @options[:limit] = l
      end
      opts.on('-n', '--dry-run', 'Run in dry-run mode.') do
        @options[:dry_run] = true
      end
      opts.on('-D', '--debug', 'Show debug logs.') do
        @options[:debug] = true
      end
    end.parse!
  end

  def check_vault_is_alive
    if @vault_client.sys.seal_status.sealed?
      @logger.warn('Vault is sealed, exiting.')
      exit 0
    end
  rescue Vault::HTTPConnectionError => e
    @logger.fatal('Error connecting to Vault')
    @logger.fatal(e)
    exit 1
  end

  def vault_auth
    vault_auth_path = ENV['VAULT_AUTH_PATH']
    vault_auth_role = ENV['VAULT_AUTH_ROLE']
    return unless vault_auth_path && vault_auth_role && KUBERNETES_SERVICE_ACCOUNT_TOKEN.exist?

    jwt = KUBERNETES_SERVICE_ACCOUNT_TOKEN.read
    auth_secret = @vault_client.logical.write("auth/#{vault_auth_path}/login", role: vault_auth_role, jwt: jwt)
    @vault_client.token = auth_secret.auth.client_token
  end

  def list_entities
    entities = []
    @vault_client.with_retries(Vault::HTTPConnectionError, Vault::HTTPServerError, attempts: VAULT_REQUEST_MAX_ATTEMPTS) do |attempt, e|
      if e
        @logger.error("Received exception from Vault - attempt #{attempt}")
        @logger.error(e)
      end
      debug('LIST /identity/entity/id')
      entities = @vault_client.logical.list('identity/entity/id')
      debug(entities)
    end
    entities
  end

  def get_entity(id)
    entity = nil
    @vault_client.with_retries(Vault::HTTPConnectionError, Vault::HTTPServerError, attempts: VAULT_REQUEST_MAX_ATTEMPTS) do |attempt, e|
      if e
        @logger.error("Received exception from Vault - attempt #{attempt}")
        @logger.error(e)
      end
      debug("GET /identity/entity/id/#{id}")
      response = @vault_client.logical.read("identity/entity/id/#{id}")
      debug(response)
      entity = response.data if response
    end
    entity
  end

  def delete_entities(ids)
    @vault_client.with_retries(Vault::HTTPConnectionError, Vault::HTTPServerError, attempts: VAULT_REQUEST_MAX_ATTEMPTS) do |attempt, e|
      if e
        @logger.error("Received exception from Vault - attempt #{attempt}")
        @logger.error(e)
      end
      @logger.info("Deleting #{ids.length} identity entities")
      debug("POST /identity/entity/batch-delete entity_ids=#{ids}")
      response = @vault_client.logical.write('identity/entity/batch-delete', entity_ids: ids)
      debug(response)
    end
  end

  def clean!
    parse_opts
    check_vault_is_alive
    vault_auth

    batch = []
    count = 0
    expiration_date = Time.now - @options[:age]

    list_entities.each do |id|
      break if @options[:limit] && count >= @options[:limit]

      entity = get_entity(id)
      if entity
        last_update_time = Time.parse(entity[:last_update_time])
        if last_update_time < expiration_date
          @logger.info("Found old identity entity id=#{id} last_update_time=#{last_update_time}")
          batch << id
          count += 1
        end
      end

      if batch.length >= @options[:batch_size]
        delete_entities(batch) unless @options[:dry_run]
        batch = []
      end
    end

    if @options[:dry_run]
      @logger.info("Found #{count} old identity entities to delete!")
    else
      delete_entities(batch) unless batch.empty?
      @logger.info("Deleted #{count} old identity entities!")
    end
  end
end

Cleaner.new.clean!
