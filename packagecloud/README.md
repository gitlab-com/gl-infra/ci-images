# Packagecloud

Packagecloud.io do not provide a Docker image so this image installs the packagecloud:enterprise Debian
package from a private repository using build time secrets.

This image is used in our [packagecloud chart](https://gitlab.com/gitlab-com/gl-infra/charts/-/tree/main/gitlab/packagecloud).
