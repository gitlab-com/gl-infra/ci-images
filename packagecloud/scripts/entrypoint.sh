#!/usr/bin/env bash

set -e

. /usr/local/lib/logging.sh

function signal_handler() {
  msg "TERM/INT signal received, trying to gracefully shutdown all services..."
  run packagecloud-ctl stop

  # Touch a file to let sidecars know that packagecloud processes have been stopped
  touch /lifecycle/main-terminated || true

  msg "Exiting ..."
}

trap "signal_handler; exit" TERM INT

if [ ! -r /gpgkey ]; then
  msg "FATAL: GPG key not found at /gpgkey"
  exit 1
fi

# Ensure this dir exists
run mkdir -p /etc/systemd/system/default.target.wants

# If running chown against mounted volumes is an issue, then
# we can copy over the files into /etc/packagecloud
if [ -f /packagecloud.rb ]; then
  run cp /packagecloud.rb /etc/packagecloud/packagecloud.rb
fi

if [ -f /users.yml ]; then
  run cp /users.yml /etc/packagecloud/users.yml
fi

# Enable Chef debuging if desired
if [ "${ENABLE_CHEF_DEBUG-0}" -eq 1 ]; then
  echo "log_level :debug" >>/opt/packagecloud/embedded/cookbooks/solo.rb
fi

msg "Starting services ..."
run /opt/packagecloud/embedded/bin/runsvdir-start &

msg "Running 'packagecloud-ctl bootstrap-install' ..."
touch /var/opt/packagecloud/bootstrapped
run packagecloud-ctl bootstrap-install

msg "Fixing permissions ..."
run chown packagecloud:packagecloud /var/opt/packagecloud

if [ -n "$BOOTSTRAP_PACKAGECLOUD_DATABASE" ]; then
  msg "Running bootstrap-database (error message is OK if DB has already been boostrapped) ..."
  run packagecloud-ctl bootstrap-database || true
fi

msg "Importing GPG private key ..."
USER=root run packagecloud-ctl gpg-private-key-import /etc/packagecloud/gpgkey </gpgkey

if grep -qE 'rds_ssl.*true' /etc/packagecloud/packagecloud.rb; then
  # This is done here as, unfortunately, if you have the embedded MySQL disabled then the
  # recipe that takes care of this doesn't get called. We want embedded MySQL disabled because
  # we're using RDS, and we need this bundle file to connect to RDS.
  msg "Copying RDS CA bundle to the right place ..."
  run install -o packagecloud -g packagecloud -m 0600 /opt/packagecloud/embedded/rds-combined-ca-bundle.pem /var/opt/packagecloud/packagecloud-rails/etc/rds-ca.pem
fi

msg "Running 'packagecloud-ctl reconfigure' ..."
run packagecloud-ctl reconfigure

touch /var/opt/packagecloud/configured
msg "Ready ..."

if [ "${USE_GITLAB_LOGGER-1}" -eq 1 ]; then
  /usr/local/bin/gitlab-logger /var/log/packagecloud &
fi

wait
