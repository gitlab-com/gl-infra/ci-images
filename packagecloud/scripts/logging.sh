#!/usr/bin/env bash

function run() {
  : "${COMPONENT:=boot}"

  if [ "${ENABLE_JSON_LOGGING-1}" -eq 1 ]; then
    "$@" 2>&1 | jq -cRM "{component: \"${COMPONENT}\", time: now | strftime(\"%Y-%m-%dT%H:%M:%SZ\"), message: .}"
  else
    "$@" 2>&1 | ts '[%Y-%m-%d %H:%M:%S]'
  fi
}

function msg() {
  run echo -e "$@"
}
