#!/usr/bin/env bash

set -euo pipefail

license_key="$(</secrets/packagecloud/enterprise_license_key)"
api_token="$(</secrets/packagecloud/packagecloud_io_api_token)"
snitch_url="$(</secrets/packagecloud/license_checker_snitch_url)"

expdate=$(curl -sSf https://"${api_token}":@packagecloud.io/api/v1/licenses/"${license_key}"/license.json | jq -r .license | yq '.":end_date"' | awk '{ print $1}')

today=$(date +%s)
notifydate=$(date -d "${expdate} - 15 days" +%s)

if [ "$today" -le "$notifydate" ]; then
  curl -s -o /dev/null -d "m=expiry date is ${expdate}" "${snitch_url}"
fi
