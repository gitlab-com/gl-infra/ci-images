# Stunnel

Small container designed to add TLS encryption functionality to existing apps without any changes.

It is being used by [packagecloud](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/packagecloud) to talk to Redis using TLS.

## Usage

```sh
docker run -p 6379:6379 \
  -v /path/to/ca.crt:/tmp/ca.crt \
  -e STUNNEL_CAFILE=/tmp/ca.crt \
  -e STUNNEL_SERVICE=redis \
  -e STUNNEL_ACCEPT=6379 \
  -e STUNNEL_CONNECT=x.x.x.x:6378 \
  stunnel:latest
```
