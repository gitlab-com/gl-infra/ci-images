#!/bin/sh -e

STUNNEL_CONF="/etc/stunnel/stunnel.conf"

# Config file replacements
set -a
: "${STUNNEL_DEBUG:=5}"
: "${STUNNEL_CLIENT:=yes}"
: "${STUNNEL_CAFILE:=/etc/ssl/certs/ca-certificates.crt}"
: "${STUNNEL_VERIFY_CHAIN:=yes}"
set +a

if [ -z "${STUNNEL_SERVICE}" ] || [ -z "${STUNNEL_ACCEPT}" ] || [ -z "${STUNNEL_CONNECT}" ]; then
  echo >&2 "At least one of the required STUNNEL_* variables is missing:"
  echo >&2 "  STUNNEL_SERVICE=${STUNNEL_SERVICE}"
  echo >&2 "  STUNNEL_ACCEPT=${STUNNEL_ACCEPT}"
  echo >&2 "  STUNNEL_CONNECT=${STUNNEL_CONNECT}"
  exit 1
fi

if [ ! -s "${STUNNEL_CONF}" ]; then
  envsubst </etc/stunnel/stunnel.conf.tpl >${STUNNEL_CONF}
fi

exec "$@"
