output = /dev/null
syslog = no
CAfile = ${STUNNEL_CAFILE}
verifyChain = ${STUNNEL_VERIFY_CHAIN}
client = ${STUNNEL_CLIENT}
pid = /var/run/stunnel/stunnel.pid
verifyChain = ${STUNNEL_VERIFY_CHAIN}
sslVersionMin = TLSv1.2
foreground = yes
debug = ${STUNNEL_DEBUG}

[${STUNNEL_SERVICE}]
accept = ${STUNNEL_ACCEPT}
connect = ${STUNNEL_CONNECT}
