# ci-images

CI images for infrastructure projects. Each directory contains a Dockerfile for
one image. [.gitlab-ci.yml](.gitlab-ci.yml) describes the pipelines that build
Docker images that are kept in this project's registry. Images are tagged:

- MR: `registry.gitlab.com/gitlab-com/gl-infra/ci-images/${image_name}:git_${short_sha}`
- MR: `registry.gitlab.com/gitlab-com/gl-infra/ci-images/${image_name}:mr_${mr_id}`
- TAG: `registry.gitlab.com/gitlab-com/gl-infra/ci-images/${image_name}:<semver>`
- TAG: `registry.gitlab.com/gitlab-com/gl-infra/ci-images/${image_name}:latest`

## Updating existing images

This project uses [Semantic Versioning](https://semver.org). We use commit
messages to automatically determine the version bumps, so they should adhere to
the standard of [Conventional Commits](https://www.conventionalcommits.org/).

1. Modify `Dockerfile`
2. Commit using conventional commits.
   - Commit messages starting with `fix:` trigger a patch version bump
   - Commit messages starting with `feat:` trigger a minor version bump
   - Commit messages with a footer `BREAKING CHANGE:` trigger a major version bump.
3. Open Merge Request

## Adding a new image

Create a directory in this repository and add a `Dockerfile` + `package.json` with image name (must match folder name, used for tag prefix).

Add image name in `.gitlab-ci.yml` on the `IMAGE` list.

Make a merge request. An image will be built and pushed to this project's
registry. When this MR is merged to master, a tag will be generated and a tagged image pushed.
