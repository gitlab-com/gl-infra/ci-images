#!/usr/bin/env bash

set -euo pipefail

if [ $# -ne 1 ]; then
  echo "Usage: $0 <image name>" >&2
  exit 1
fi

image_name="$1"

curl_api() {
  curl --fail --show-error --silent --header "${authorization_header}" "$@"
}

urlencode() {
  echo -n "$@" | jq -srR @uri
}

if [ -z "${GITLAB_API_TOKEN:-}" ]; then
  echo "FATAL: GitLab API token not found in env variable GITLAB_API_TOKEN"
  exit 1
fi

api_base_url="https://ops.gitlab.net/api/v4"
authorization_header="Authorization: Bearer ${GITLAB_API_TOKEN}"
project_path="$(urlencode "${CI_PROJECT_PATH}")"
api_project_url="${api_base_url}/projects/${project_path}"

pipeline_id=$(curl_api "${api_project_url}/pipelines?per_page=100" | jq -r --arg sha "${CI_COMMIT_SHA}" --arg ref "${CI_COMMIT_REF_NAME}" '[.[] | select(.sha == $sha and .ref == $ref)] | first | .id | select (. != null)')

if [ -z "${pipeline_id}" ]; then
  echo "No pipelines found on '${api_project_url}' for sha '${CI_COMMIT_SHA}' and ref '${CI_COMMIT_REF_NAME}'"
  exit 0
fi

echo "Pipeline ID: ${pipeline_id}"

job_id=$(curl_api "${api_project_url}/pipelines/${pipeline_id}/jobs" | jq -r --arg image "${image_name}" '[.[] | select((.name | test("^container_scanning:\\s\\[" + $image + ",")) and .artifacts_file != null)] | first | .id | select (. != null)')

if [ -z "${job_id}" ]; then
  echo "No container scanning job found for pipeline ID ${pipeline_id} and image '${image_name}'."
  exit 0
fi

echo "Fetching artifacts for job ID ${job_id}..."
artifact_file="artifacts-${job_id}.zip"
curl_api -L -o "${artifact_file}" "${api_project_url}/jobs/${job_id}/artifacts"
unzip -o "${artifact_file}"
